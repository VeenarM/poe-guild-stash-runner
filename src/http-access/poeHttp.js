const guildProfileId = process.env.GUILD_PROFILE_ID;
const version = require('../../package').version;
const fetch = require('node-fetch2');
const valvelet = require('valvelet');
const logger = require('../logger').getLogger();

logger.info(`PoeHttpAccess has configured the Cookie Jar.`);

const opts = {
  json: true,
  headers: {
    'User-Agent': `NodeJS node-fetch poe-stash-runner/${version} (Gitlab/@VeenarM)`,
    cookie: `POESESSID=${process.env.POESESSID};`
  }
}

async function fetchMostRecentHistory() {
  const encodedUrl = encodeURI(`https://www.pathofexile.com/api/guild/${guildProfileId}/stash/history`);
  logger.debug(`Fetching History: ${encodedUrl}`);

  return await fetch(encodedUrl, opts).then((res) => {
    logger.debug(`Status: ${res.status}`);
    return res.json();
  });
}

/**
 * Rate Limited Function
 * Defaults: 2 requests, per 5 second period.
 */
const fetchHistory = valvelet(fetchHistoryNonLimited, 2, 5000, 500);

// Then you fetch history rolling backwards...
async function fetchHistoryNonLimited(fromEpochTime, fromId) {
  const encodedUrl = encodeURI(
    `https://www.pathofexile.com/api/guild/${guildProfileId}/stash/history?from=${fromEpochTime}&fromid=${fromId}`
  );
  logger.debug(`Fetching History: ${encodedUrl}`);

  return await fetch(encodedUrl, opts).then((res) => {
    logger.debug(`Status: ${res.status}`);
    return res.json();
  });
}

module.exports = {
  fetchHistory,
  fetchMostRecentHistory
};
