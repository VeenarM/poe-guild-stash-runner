const discordMessageChannel = process.env.DISCORD_MESSAGE_CHANNEL;
const logger = require('./logger').getLogger();
const { Client, GatewayIntentBits } = require('discord.js');
const discordClient = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages] });
const _ = require('lodash');
const valvelet = require('valvelet');


logger.info(`DiscordFileLog has been initialized.`);

discordClient.on('error', (err) => {
  logger.error(`FATAL =>  ${err.message}`);
});

discordClient.on('reconnecting', () => {
  logger.info(`Attempting WS reconnection...`);
});

async function doLogin() {
  return login().catch((error) => {
    throw Error(error.message);
  });
}

const login = _.throttle(() => {
  logger.info(`Discord.js - Attempting to login`);
  return discordClient
    .login(process.env.DISCORD_TOKEN)
    .then(() => {
      logger.info(`Discord.js - Login successful - now caching stash channel`);
    })
    .catch((err) => {
      logger.error(`Discord.js - Exception logging in - ${err}`);
      doLogin().catch((err) => {
        throw Error(err.message);
      });
      throw err;
    });
}, 10000);

/**
 * Rate Limited Function
 * Defaults: 40 requests, per 1000milliseconds period.
 * You could in theory bump this to I believe 50 per second, but figured it's not overly important and better to not risk throttling.
 */
const sendMessageToGuildStashChannel = valvelet(sendMessageToGuildStashChannelUnthrottled, 40, 1000);

function sendMessageToGuildStashChannelUnthrottled(message) {
  discordClient.channels.fetch(discordMessageChannel).then((ch) => {
    ch.send(message).catch((err) => {
      logger.error(err);
    });
  });
}

function sendMessageToGuildStashThenRemove(message) {
  discordClient.channels.fetch(discordMessageChannel).then((ch) => {
    ch.send(message)
      .then((msg) => {
        msg
          .delete({ timeout: 10000 })
          .then((deletedMessage) => {
            logger.debug(`Deleted message from ${deletedMessage.author.username}`);
          })
          .catch((err) => {
            logger.error(err);
          });
      })
      .catch((err) => {
        logger.error(err);
      });
  });
}

module.exports = {
  sendMessageToGuildStashChannel,
  doLogin,
  sendMessageToGuildStashThenRemove
};
