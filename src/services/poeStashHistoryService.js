const GuildStashTab = require('../data-access/guildStashTab');
const PoeHttp = require('../http-access/poeHttp');
const logger = require('../logger').getLogger();
const _ = require('lodash');
const DiscordHelper = require('../discordHelper');
const moment = require('moment');
const discordEnabled = (process.env.DISCORD_ENABLED === 'true');

let processingRecords = false;

async function processRecords() {
  if (processingRecords) {
    logger.info('Still processing previous records, no action this run, will wait til next run.');
    return;
  }

  const hrstart = process.hrtime();

  processingRecords = true;

  let allRecords = [];

  await PoeHttp.fetchMostRecentHistory()
    .then(async (jsonResponse) => {
      if (!jsonResponse || !jsonResponse.entries || !jsonResponse.entries.length === 0) {
        logger.info('No records received for processing (should only happen when server is down)');
        processingRecords = false;
        return;
      }

      let lastRecordedRecord;
      await GuildStashTab.findLatestRecord()
        .then((lr) => {
          lastRecordedRecord = lr[0];
        })
        .catch((err) => {
          logger.warn(err);
        });

      const firstRun = !lastRecordedRecord;
      if (firstRun) {
        lastRecordedRecord = { id: 0, time: 0 }; // Means start from scratch really..
      }

      logger.info(`LAST RECORDED RECORD = id[${lastRecordedRecord.id}], epoch[${lastRecordedRecord.time}], dt[${moment.unix(lastRecordedRecord.time).format("YYYY-MM-DD hh:mm A")}]`);

      let continueProcessing = true;
      let loopResult = loopProcess(jsonResponse.entries, lastRecordedRecord);
      continueProcessing = loopResult.continueProcessing;

      if (_.size(loopResult.entries) > 0) {
        allRecords = allRecords.concat(loopResult.entries);
        await GuildStashTab.insertMany(loopResult.entries).then((doc) => {
          if (doc && doc.insertedIds) {
            logger.info(_.size(doc.insertedIds) + ' records inserted');
          } else {
            logger.info(`No records inserted`);
          }
        });
      }

      while (continueProcessing) {
        if (_.size(loopResult.entries) > 0) {
          const last = _.last(loopResult.entries);
          await PoeHttp.fetchHistory(last.time, last.id).then(async (jr) => {
            if (!jr || !jr.entries || !jr.entries.length === 0) {
              continueProcessing = false;
              logger.info('No records received for processing');
            } else {
              loopResult = loopProcess(jr.entries, lastRecordedRecord);
              if (_.size(loopResult.entries) > 0) {
                allRecords = allRecords.concat(loopResult.entries);
                await GuildStashTab.insertMany(loopResult.entries).then((d) => {
                  logger.info(_.size(d.insertedIds) + ' records inserted');
                });
              } else {
                logger.info(`No more records to process more entries`);
              }
            }
          });
        }

        continueProcessing = loopResult.continueProcessing;
      }

      // Send to discord if enabled
      if (discordEnabled) {
        if (_.size(allRecords) > 0) {
          for (let i = allRecords.length - 1; i >= 0; i--) {
            const dt = new Date(0);
            dt.setUTCSeconds(allRecords[i].time);
            const formattedDate = moment(dt).format('YYYY/MM/DD hh:mm:ss A');
            DiscordHelper.sendMessageToGuildStashChannel(
              `${formattedDate}: ${allRecords[i].account.name} **${allRecords[i].action}** item '${allRecords[i].item}' in **${allRecords[i].league} League**`
            ).catch((e) => logger.warn(`sending message to discord failed ${e}`));
          }
        }
      }
    })
    .catch((err) => {
      if (discordEnabled) {
        DiscordHelper.sendMessageToGuildStashThenRemove(`API Call Failed Status Code: ${err.statusCode || err}`);
      }
      logger.error(`API Call Failed Status Code: ${err.statusCode || err}`);
    })
    .finally(() => {
      processingRecords = false;
      const hrend = process.hrtime(hrstart);
      logger.info(`Execution time: ${hrend[0]}s ${hrend[1] / 1000000}ms`);
    });
}

function loopProcess(responseEntries, lastRecordedRecord) {
  const entries = [];
  let continueProcessing = true;
  if (_.size(responseEntries) > 0) {
    for (let i = 0; i < _.size(responseEntries); i++) {
      logger.debug(
        `RecordId = ${responseEntries[i].id}/${responseEntries[i].time} vs ${lastRecordedRecord.id}/${
          lastRecordedRecord.time
        } == ${responseEntries[i].id == lastRecordedRecord.id}`
      );
      if (responseEntries[i].id == lastRecordedRecord.id) {
        continueProcessing = false;
        logger.debug(`No more records to processing, setting continueProcessing to false.`);
        break;
      } else {
        entries.push(responseEntries[i]);
      }
    }
  } else {
    continueProcessing = false;
    logger.debug(`No more records to processing, setting continueProcessing to false.`);
  }
  logger.debug(`Continue processing = ${continueProcessing}`);
  return { entries: entries, continueProcessing: continueProcessing };
}

module.exports = {
  processRecords,
};
