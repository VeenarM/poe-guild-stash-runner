const winston = require('winston');

const logToFile = process.env.LOG_TO_FILE ? process.env.LOG_TO_FILE : 'true';
const logAsJson = process.env.LOG_AS_JSON ? process.env.LOG_AS_JSON : 'false';

const transports = [];

if (logAsJson === 'true') {
  transports.push(
    new winston.transports.Console({
      format: winston.format.json()
    })
  );
} else {
  const consoleFormat = winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss.SSS'
    }),
    winston.format.printf((info) => `${info.timestamp} [${info.level}]: ${info.message}`)
  );
  const consoleTransport = new winston.transports.Console({
    format: consoleFormat
  });
  transports.push(consoleTransport);
}
if (logToFile === 'true') {
  const fileFormat = winston.format.combine(
    winston.format.colorize(),
    winston.format.uncolorize(),
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss.SSS'
    }),
    winston.format.printf((info) => `${info.timestamp} [${info.level}]: ${info.message}`)
  );
  transports.push(
    new winston.transports.File({ dirname: 'logs', filename: 'error.log', level: 'error', maxsize: '5m', format: fileFormat }),
    new winston.transports.File({ dirname: 'logs', filename: 'combined.log', maxsize: '5m' , format: fileFormat})
  );
}

const logger = winston.createLogger({
  level: process.env.LOG_LEVEL,
  defaultMeta: { service: 'guild-stash-service' },
  transports: transports
});

function getLogger() {
  return logger;
}

module.exports = {
  getLogger
};
