const MongoClient = require('mongodb').MongoClient;
const exileDbName = process.env.MONGO_DB_NAME;

let MONGO_URI = process.env.MONGO_DB_URL;

if (!MONGO_URI) {
  MONGO_URI = `mongodb+srv://${process.env.MONGO_DB_USER}:${process.env.MONGO_DB_PASS}@${process.env.MONGO_DB_HOST}/${exileDbName}`;
} else {
  MONGO_URI += MONGO_URI.endsWith('/') ? `${exileDbName}` : `/${exileDbName}`;
}

let _connection;
const connectDB = async (callback) => {
  try {
    await MongoClient.connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
      _connection = db;
      return callback(err);
    });
  } catch (e) {
    throw e;
  }
};

const getDB = () => _connection.db(exileDbName);

const disconnectDB = () => _connection.close();

module.exports = {
  connectDB,
  getDB,
  disconnectDB,
};
