const MongoDB = require('./mongoDb');
const guildStashTabCollection = process.env.MONGO_DB_COLLECTION ? process.env.MONGO_DB_COLLECTION : 'guildStashLog';


const insertMany = async (entries) => {
  try {
    return await MongoDB.getDB().collection(guildStashTabCollection).insertMany(entries);
  } catch (e) {
    throw e;
  }
};

const insert = async (entry) => {
  try {
    return await MongoDB.getDB().collection(guildStashTabCollection).insertOne(entry);
  } catch (e) {
    throw e;
  }
};

const findAll = async () => {
  try {
    return await MongoDB.getDB().collection(guildStashTabCollection).find({}).toArray();
  } catch (e) {
    throw e;
  }
};

const findLatestRecord = async () => {
  try {
    return await MongoDB.getDB().collection(guildStashTabCollection).find({}).sort({ time: -1, id: -1 }).limit(1).toArray();
  } catch (e) {
    throw e;
  }
};

module.exports = {
  insertMany,
  insert,
  findAll,
  findLatestRecord,
};
