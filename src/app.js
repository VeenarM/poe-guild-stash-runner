
require('dotenv').config() // Uncomment for local dev only
const logger = require('./logger').getLogger();
const MongoDB = require('./data-access/mongoDb');
const DiscordHelper = require('./discordHelper');
const PoeStashHistoryService = require('./services/poeStashHistoryService');
const { sendMessageToGuildStashThenRemove } = require('./discordHelper');

const processInterval = process.env.PROCESS_INTERVAL_MS ? process.env.PROCESS_INTERVAL_MS : 90000;

// Initialize the bot now connection DB connection established.
MongoDB.connectDB((err) => {
  if (err) {
    logger.error(err);
    throw err;
  }
  logger.info(`DB Connected...`);

  if (process.env.DISCORD_ENABLED === 'true') {
    logger.info(`Discord Integration Enabled`);
    DiscordHelper.doLogin()
      .then(() => {
        logger.info(`Discord Connected - Starting processing records...`);
        sendMessageToGuildStashThenRemove(`Discord Connected - Starting processing records...`);
        PoeStashHistoryService.processRecords();
        setInterval(PoeStashHistoryService.processRecords, processInterval);
      })
      .catch(() => {
        logger.error(`Discord.js - Exception logging in - Not running app just yet.`);
      });
  } else {
    logger.info(`Discord Integration Disabled`);
    PoeStashHistoryService.processRecords();
    setInterval(PoeStashHistoryService.processRecords, processInterval);
  }
});
