# Path of Exile / Guild Stash Tab Exporter

## How it works

This is a NodeJS application which requires a MongoDB for data storage.
It optionally can paste out results to a Discord server channel.

**Important** It's recommended to disable discord integration the first time you run the program as it gets **ALL** of your history and pumps it out. Then turn on integration during a league etc to see things live as they occur.

**Latest** Image Version is 2.1.1


## Required Environment Settings

| Variable | Value | Comments |
| ---      | ---   | ---      |
| GUILD_PROFILE_ID | "510224" | Your poe guild id (This is your GuildID as shown in the browser when you're on your guild page in pathofexile.com) |
| TZ | "Australia/Sydney" | Your default timezone |
| LOG_LEVEL | "info" | (info,debug,warn,error) |
| LOG_TO_FILE | true | Dictates if files are written to the app/logs directory, you may not want this in a container env |
| LOG_AS_JSON | false | Modifies the Console out log files, default is simple, but in a container you may want json for elastic |
| PROCESS_INTERVAL_MS | "90000" | (default 1.5min) How often to scrape from `pathofexile.com` in milliseconds, if you run two instances i'd spread this out for the API (e.g. poe1 and poe2) |
| DISCORD_ENABLED | "true" | `true` or `false` if you want discord messaging enabled.|
| DISCORD_MESSAGE_CHANNEL | "567374568743453453" | The channel `UID` to send messages too (required, if `DISCORD_ENABLED` = `true`) |
| MONGO_DB_HOST | "yourhost.mongodb.net" | Required if not using `MONGO_DB_URL` as a secret which embeds the user/pass/host in a single line. |
| MONGO_DB_NAME | "heroku_8g3a0l" | The database name you created. |
| MONGO_DB_COLLECTION | "guildStashLog" | The mongo collection table you created.|
| MONGO_DB_URL |  "mongodb+srv://dbuser:dbpass@yourhost.mongodb.net" | Your full length URL to connect to mongo server, if you use this you don't need the `HOST/USER/PASS` variables defined. If using k8s recommended to use `Secrets`  |
| MONGO_DB_USER | "dbuser" | Your database `username` if using k8s recommended to use `Secrets` |
| MONGO_DB_PASS | "dbpass" | Your database `password` if using k8s recommended to use `Secrets` |
| DISCORD_TOKEN | "" | Your generated discord API token from the BOT / Dev Area, if using k8s recommended to use `Secrets` |
| POESESSID | "" | Your POESESSID used to access your account on the POE Website to obtain the stash data, if using k8s recommended to use `Secrets`  |

## Installation / Run (To be updated for Docker/K8s)
- Update Environment Variables
  - Retrieve your POESESSID from POE Website (use dev tools to locate or google it)
  - DISCORD_TOKEN - required for the bot to connect [discord dev portal](https://discordapp.com/developers/docs/intro)
  - MONGO_DB_URL - if you want to use this or config.json (use this is safer...) The URI shouldn't include the database on the end as we add it through the setting of mongoDB in config.
- Manage your Discord Server,
  - Create a role you want that has permissions to a channel to post messages.
  - Create a #stash-log chat channel, I have it locked for everyone (reading only, no writing except for the bot user role who can write.
  - Get the ID's of the channel and add it to the config.json
- Set your timezone to match that of whom ever is setting up the bot to there 'PoE' timezone in the website. This makes dates/times match
- **Above is required** even if it's UTC, because PoE add 'Z' to the end of the last_online, even though they actually are returned in local settings time of the user.

