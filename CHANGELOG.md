# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.1] 2024-12-29
- Fix: Added extra sort condition to 'get latest record' to sort by time and record id

## [2.1.0] 2024-12-29
- Added Extra logger settings
- Added New defaults for certain values
- Confirmed works with PoE2 Guild Stashes
- Increased default api calls for discord
- Fixed UserAgent version setting
- Replaced `SimpleNodeLogger` with `winston@v3`
- Replaced `request-promise-native` with `node-fetch2`
- Rebuilt on `alpine` with node version 23
- Added a example docker compose file to the repository
- Added ability locally to use a `.env` file with minor adjustment. (LOCAL ONLY)


## [2.0.0] - 2020-09-09

- Overhauled the whole project for conformity.
  - added linting/prettier configs
  - updated everything to new latest information
  - config expanded to enable/disable discord messages (if you just want the DB aspects)

## [1.0.0] - 2020-06-23

### Released

- Initial release - save/records stash log entries to mongo db, you can also trigger whether you want it to post to your discord channel too!
